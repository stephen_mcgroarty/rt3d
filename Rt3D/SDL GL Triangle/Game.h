#ifndef GAME_H
#define GAME_H
#include "FileReader.h"
#include <map>
#include "Renderable.h"
#include <vector>
#include "Level.h"


class Game{
public:
	Game();
	void init();
	void start();


	static GLuint loadBitmap(std::string fileName);

	static SDL_Window * setupRC(SDL_GLContext &context);

	static bool isRunning() { return running;}
	static void checkInput();
	
	static std::map<std::string,GLuint> & getShaders() { return shaders;}
	static std::map<std::string,GLuint> & getTextures() { return textures;}
	static std::map<std::string,rt3d::materialStruct> & getMaterials() { return materials; }

	static GLuint *& getActiveTexture() { return activeTexture; }
	static std::string & getActiveMaterial() { return activeMaterial; }
	static GLuint & getActiveShader() { return activeShader; }
	static std::vector<GLuint> & getBuildingTextures() { return buildingTetxures; }
private:
	static FileReader fileIO;
	static bool running;

	static SDL_Window * hWindow;
    static SDL_GLContext glContext; 


	static void draw(SDL_Window * window);

	static std::map<std::string,GLuint> shaders;
	static std::map<std::string,GLuint> textures;
	static std::map<std::string,rt3d::materialStruct> materials;
	static std::vector<GLuint> buildingTetxures; // these point to the textures for buildings in the map 
	static GLuint activeShader;
	static GLuint * activeTexture;
	static Level * currentLevel;
	static std::string  activeMaterial;
	static std::vector<Level> levels;
};

#endif