#include "Renderable.h"
#include "rt3dObjLoader.h"
#include <vector>
#include "Camera.h"
#include "Game.h"

std::map<std::string,std::pair<GLuint,GLuint>> Renderable::map;



template<typename type> // converts a vector to an array
static type * toArray(std::vector<type>& vec)
{
	type * arr;

	if( vec.size() != 0 )
		arr = new type[vec.size()];
	else
	{
		arr = nullptr;
		return arr;
	}
	
	for(int i =0 ;i <vec.size(); i++)
		arr[i] = vec[i];
	return arr;
}


Renderable::Renderable(GLfloat * vertices,GLfloat * texCoords,GLuint * ind,GLuint iCount,GLuint vCount)
{

	mesh = rt3d::createMesh(vCount,vertices,nullptr,vertices,texCoords,iCount,ind);
	indiceCount = iCount;
	position = glm::vec3(1.0f,1.0f,1.0f);
	scale = glm::vec3(1.0f,1.0f,1.0f);
	angle = 0;
	rotationalAxis = glm::vec3(0.0f,1.0f,0.0f);
	cantMove = cantScale = false;
	angle =0;
	model = nullptr;

}


Renderable::Renderable(GLfloat ix,GLfloat iy,GLfloat iz,std::string filePath): position(glm::vec3(ix,iy,iz) )
{
	rotationalAxis = glm::vec3(0.0f,1.0f,0.0f); //default 
	scale = glm::vec3(1.0f,1.0f,1.0f);
	cantMove = cantScale = false;
	angle = 0;
	dx = dy = dz = 0;
	model = nullptr;
	
	material =  "Default";
	currentAnimation = 0;


	if(map.find(filePath) != map.end() )
	{
		std::pair<GLuint,GLuint> node = map[filePath];
		mesh = node.first;
		indiceCount = node.second;
		return;
	}

	std::string::iterator it = filePath.end() - 3;
	std::string result = "";

	for(;it != filePath.end(); it++)
		result+= *it;


	if( result.compare("obj" ) == 0 )
		initObj(filePath);
	else if( result.compare("md2") == 0 )
		initMd2(filePath);


}


void Renderable::initMd2(std::string filePath)
{
	model = new md2model( );
	mesh = model->ReadMD2Model( filePath.c_str() );
	vertexCount = model->getVertDataSize();
}

void Renderable::setMaterial(std::string& str)
{
	material = str;
}


void Renderable::initObj(std::string filePath)
{

	std::vector<GLfloat> normals,vertices,colours,textureCordinates;
	std::vector<GLuint> indi;


	
	rt3d::loadObj(filePath.c_str(),vertices,normals,textureCordinates,indi);

	mesh = rt3d::createMesh(vertices.size(),toArray<GLfloat>(vertices),toArray<GLfloat>(colours),toArray<GLfloat>(normals),
		toArray<GLfloat>(textureCordinates),indi.size(),toArray<GLuint>(indi));
	

	std::pair<GLuint,GLuint> n;
	n.first = mesh;
	n.second = indi.size();

	map[filePath] = n;

	indiceCount = indi.size();
	
	
	normals.clear();
	vertices.clear();
	colours.clear();
	textureCordinates.clear();
	indi.clear();
}






void Renderable::drawWithRefMat(GLuint & mvpShaderProgram,glm::mat4 & modelView)
{
	if(  Game::getActiveTexture() ==  nullptr || *Game::getActiveTexture() != texture )
	{
		glBindTexture(GL_TEXTURE_2D,texture);	
		Game::getActiveTexture() = &texture;
	}

	if( Game::getActiveMaterial().compare( material ) != 0 )
	{
		Game::getActiveMaterial() = material;
		rt3d::setMaterial(mvpShaderProgram,Game::getMaterials()[material]);
	}

	if( model != nullptr ) glCullFace(GL_FRONT);



	if( !cantMove ) modelView = glm::translate(modelView, position);
	if( model != nullptr) modelView = glm::rotate(modelView,90.0f,glm::vec3(-1.0f,0.0f,0.0f) );
	modelView = glm::rotate(modelView, angle,rotationalAxis);
	if( !cantScale )modelView = glm::scale(modelView,scale);
	
	rt3d::setUniformMatrix4fv(mvpShaderProgram, "modelview", glm::value_ptr(modelView));
	if( model == nullptr )rt3d::drawIndexedMesh(mesh,indiceCount,GL_TRIANGLES);
	else rt3d::drawMesh(mesh,vertexCount,GL_TRIANGLES);


	if( model != nullptr ) glCullFace(GL_BACK);
}

void Renderable::draw(GLuint& mvpShaderProgram,glm::mat4 modelView)
{
	drawWithRefMat(mvpShaderProgram,modelView);
}

void Renderable::setScale(glm::vec3 s)
{
	scale = s;
}

void Renderable::setTexture(GLuint tex)
{
	texture = tex;
}

void Renderable::setAngle(float a)
{
	angle = a;
}

void Renderable::moveTo(float nx,float ny,float nz,float nAngle)
{
	position = glm::vec3(nx,ny,nz);
	angle = nAngle;
}


void Renderable::handleInput(Uint8 * keys)
{
//	if( keys[SDL_SCANCODE_W] ) z += 0.1f;
//	if( keys[SDL_SCANCODE_S] ) z -= 0.1f;
//	if( keys[SDL_SCANCODE_A] ) x -= 0.1f;
//	if( keys[SDL_SCANCODE_A] ) x += 0.1f;
}


void Renderable::setRotationalAxis(glm::vec3 a)
{
	rotationalAxis = a;
}


void Renderable::update(Camera cam)
{
	if( model != nullptr) 
	{
		model->Animate(currentAnimation,0.1);
		rt3d::updateMesh(mesh,RT3D_VERTEX,model->getAnimVerts(),model->getVertDataSize());
	}
}







bool Renderable::checkCollision(Renderable & col)
{
	float thisWidth,thisHeight,colWidth,colHeight; // height in this case is Z axis
	
	if( this->getCantScale() )
		thisWidth = thisHeight = 1.0f;
	else 
	{
		thisWidth = this->getScale().x;
		thisHeight = this->getScale().z;
	}

	if( col.getCantScale() )
		colWidth = colHeight = 1.0f;
	else 
	{
		colWidth = col.getScale().x;
		colHeight = col.getScale().z;
	}

	// shift X and Z cords to left hand side
	float thisX = this->getX() - 1.0f*thisWidth;
	float thisZ = this->getZ() - 1.0f*thisHeight;
	float colX = col.getX() - 1.0f*colWidth;
	float colZ = col.getZ() - 1.0f*colHeight;


	if( thisX > colX + 2.0f*colWidth ) return false;
	if( thisX + 2.0f*thisWidth < colX ) return false;
	if( thisZ > colZ + 2.0f*colHeight ) return false;
	if( thisZ+ 2.0f*thisHeight < colZ ) return false;
	return true;

}