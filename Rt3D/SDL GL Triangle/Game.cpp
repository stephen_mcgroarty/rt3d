#include "Game.h"
#include <iostream>
#include <sstream>
std::map<std::string,GLuint> Game::shaders;
std::map<std::string,GLuint> Game::textures;
std::map<std::string,rt3d::materialStruct> Game::materials;


GLuint Game::activeShader;
Level * Game::currentLevel;
std::vector<Level> Game::levels;

bool Game::running;
SDL_Window * Game::hWindow;
SDL_GLContext Game::glContext;
GLuint * Game::activeTexture;
std::string Game::activeMaterial = "Default";

static rt3d::lightStruct light0 = {
{1.0f, 1.0f, 1.0f, 1.0f}, // ambient
{0.7f, 0.7f, 0.7f, 1.0f}, // diffuse
{0.8f, 0.8f, 0.8f, 1.0f}, // specular
{0.0f, 0.0f, 0.0f, 0.0f} // position
};
static rt3d::materialStruct transparent = {
{0.0f, 1.0f, 0.0f, 0.5f}, // ambient
{0.5f, 0.5f, 0.5f, 0.0f}, // diffuse
{0.8f, 0.8f, 0.8f, 0.0f}, // specular
2.0f // shininess
};

static rt3d::materialStruct transparentRed = {
{1.0f,0.0f,0.0f,0.5f },
{0.5f, 0.5f, 0.5f, 0.0f}, // diffuse
{0.8f, 0.8f, 0.8f, 0.0f}, // specular
2.0f // shininess
};

static rt3d::materialStruct defaultMaterial = {
{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
{0.8f, 0.8f, 0.8f, 1.0f}, // specular
2.0f // shininess
};

std::vector<GLuint> Game::buildingTetxures;

static float angle;
Game::Game()
{
	angle = 0;
	activeTexture = nullptr;
}


GLuint Game::loadBitmap(std::string fileName)
{

	const char * fname = fileName.c_str();
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID
	// load file - using core SDL library
	SDL_Surface *tmpSurface = SDL_LoadBMP(fname);

	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
	externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}
	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
	externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID; // return value of texture ID
}

SDL_Window * Game::setupRC(SDL_GLContext &context)
{
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
  
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	glEnable(GL_DEPTH_TEST);
	return window;

}


void Game::init(void)
{
	hWindow = setupRC(glContext); 
	
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << std::endl;
		exit (1);
	}

	running = true; // set running to true


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	shaders.insert(shaders.begin(),std::pair<std::string,GLuint>("phong-tex",rt3d::initShaders("phong-tex.vert","phong-tex.frag") ));
	shaders.insert(shaders.begin(),std::pair<std::string,GLuint>("Skybox",rt3d::initShaders("Textured.vert","Textured.frag")));

	activeShader = shaders["phong-tex"];
	glUseProgram(shaders["phong-tex"]);



	materials["Transparent"] = transparent;
	materials["Default"] = defaultMaterial;


	rt3d::setLight(shaders["phong-tex"],light0);
	rt3d::setMaterial(shaders["phong-tex"],defaultMaterial);

	
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);

	//textures.insert( textures.begin(),std::pair<std::string,GLuint>("fabirc",      loadBitmap("fabric.bmp")));

	//textures.insert( textures.begin(),std::pair<std::string,GLuint>("unionFlag",   loadBitmap("union_flag.bmp")));
	textures.insert( textures.begin(), std::pair<std::string,GLuint>("Hobgoblin",	 loadBitmap("Hobgoblin.bmp")));
	textures.insert( textures.begin(), std::pair<std::string,GLuint>("road",       loadBitmap("road.bmp") ));
	textures.insert( textures.begin(),std::pair<std::string,GLuint>("crossroad",   loadBitmap("roadX.bmp")));
	//textures.insert( textures.begin(),std::pair<std::string,GLuint>("roadYCross",  loadBitmap("roadY.bmp")));
	textures.insert( textures.begin(),std::pair<std::string,GLuint>("BuildingFace",loadBitmap("BuildingFace1.bmp")));
	
	buildingTetxures.push_back( textures["BuildingFace"] );
	
	textures.insert( textures.begin(),std::pair<std::string,GLuint>("BuildingGlass",loadBitmap("buildingGlass.bmp")));
	buildingTetxures.push_back( textures["BuildingGlass"] );

	textures.insert( textures.begin(),std::pair<std::string,GLuint>("concrete",loadBitmap("concrete.bmp")));
	buildingTetxures.push_back( textures["concrete"] );

//	textures.insert( textures.begin(),std::pair<std::string,GLuint>("DarkBrick",loadBitmap("DarkBrick.bmp")));
//	buildingTetxures.push_back( textures["DarkBrick"] );
	textures.insert( textures.begin(),std::pair<std::string,GLuint>("grass",       loadBitmap("grass.bmp")));

	std::hash_map<std::string,Renderable> renderables;
	 


	int number = 1;
	for(int i = -100; i < 100; i+=10)
	{
		for( int j = -100; j < 100; j+=10)
		{
			std::ostringstream stream;
			stream << number++;
			
			std::string s = "Ground" + stream.str();

			renderables[s] =  Renderable(i,0.0f,j,"Cube.obj");

			
			
			if( i == 0 && j == 0 ) renderables[s].setTexture( textures["crossroad"] );
			else if( i == 0 || j ==0 ) renderables[s].setTexture(textures["road"]);
			else renderables[s].setTexture(textures["grass"]);
			renderables[s].setAngle(90);
		
			if( i == 0 ) renderables[s].setAngle(180); 
			
		
			renderables[s].setScale(glm::vec3(5.0f,0.0001f,5.0f));
		}
	}
	
	renderables["Player"] = ( Renderable(0.0f,1.15f,0.0f,"tris.md2") );
	renderables["Player"].setScale(glm::vec3(0.05f,0.05f,0.05f));
	renderables["Player"].setRotationalAxis(glm::vec3(0.0f,0.0f,1.0f));
	renderables["Player"].setAngle(90.0f);
	renderables["Player"].setTexture(textures["Hobgoblin"] );

	
	

	levels.push_back( Level(renderables) );
	currentLevel = &levels.back();
	levels.back().getCameras().insert(levels.back().getCameras().begin(),std::pair<std::string,Camera>("Free",Camera()));
	levels.back().getCameras()["Free"].getEye() = glm::vec3(-10.0f,3.0f,-10.0f);


	levels.back().getCameras()["Follow"] = Camera();
	levels.back().getCameras()["Follow"].getEye() = glm::vec3(10.0f,40.0f,10.0f);

	levels.back().getCameras()["Fixed"] = Camera();
	levels.back().getCameras()["Fixed"].getEye() = glm::vec3(0.0f,100.0f,0.f);
	levels.back().getCameras()["Fixed"].getAt() = glm::vec3(0.0f,0.0f,0.0f);


	materials["TransparentRed"] = transparentRed;


	

}

void Game::start()
{

	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		currentLevel->update();
		checkInput();
		draw(hWindow); // call the draw function
	} 

	SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
}

void Game::checkInput()
{
	 Uint8*keys=SDL_GetKeyboardState(NULL);
	 currentLevel->handleKeyBoardInput(keys);
}

void Game::draw(SDL_Window * window)
{
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	currentLevel->draw();
	

    SDL_GL_SwapWindow(window); // swap buffers

}