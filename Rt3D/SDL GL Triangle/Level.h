#ifndef LEVEL_H
#define LEVEL_H
#include "Renderable.h"
#include <vector>
#include "Camera.h"
#include "Skybox.h"

#include <hash_map>


class Level{
public:
	Level(std::hash_map<std::string,Renderable>& objs);

	void update();
	void draw();
	void handleKeyBoardInput(Uint8 * keys);

	std::hash_map<std::string,Camera> & getCameras() { return cameras;}
	std::hash_map<std::string,Renderable> & getRenderables() { return renderableObjects;}

	Skybox & getSkybox() { return *skybox;}
private:
	std::hash_map<std::string,Renderable> renderableObjects;
	std::hash_map<std::string,Camera> cameras;
	Skybox *skybox;
	std::string activeCamera;
	glm::vec4 lightPosition;
	Renderable * buildingTemplate;

	bool canMove;
	
	static int buildingsPlaced;
	float buildingCameraDistance;
	int buildingTextureIndex;
	bool placeable;

};


#endif