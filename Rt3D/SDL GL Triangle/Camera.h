#ifndef CAMERA_H
#define CAMERA_H
#include "Renderable.h" // for GLM stuff

class Renderable;

class Camera
{
public:
	Camera();
	Camera(glm::vec3 eye,glm::vec3 center,glm::vec3 up);

	glm::mat4 look(); // glm::lookAt with private members
	// the new center to look towards
	void focusOn(glm::vec3 focusCenter); 
	void move(glm::vec3 diffrence);
	//void rotate(float angle);
	void moveTo(glm::vec3 newPosition);

	void moveZPlane(float distance);
	void moveXPlane(float distance);


	void handleInput(unsigned char key);

	static glm::vec3 moveZPlane(glm::vec3 cam, float angle,float distance);
	static glm::vec3 moveXPlane(glm::vec3 cam, float angle,float distance);

	float & getAngle() { return angle;}
	
	glm::vec3 & getEye() { return eye;}
	glm::vec3 & getAt() {return center;}
	glm::vec3 & getUp() { return up;}
private:
	glm::vec3 eye,up,center; // center of object to look at

	bool locked; // is camera locked to object?
	float angle;
	
};

#endif