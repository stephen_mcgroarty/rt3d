#include "Skybox.h"

GLfloat Skybox::vertices[] =
{
	-2.0f,-2.0f, 2.0f,
	-2.0f,2.0f,2.0f,
	2.0f,2.0f,2.0f,
	2.0f,-2.0f,2.0f
};


GLfloat Skybox::textCoord[] =
{
	0.0f,0.0f,
	1.0f,0.0f,
	1.0,1.0f,
	0.0f,1.0f
};

GLuint Skybox::indices[] =
{
	0,1,
	2,2,
	3,0
};

GLuint Skybox::vertexCount =4 , Skybox::indiceCount = 6;

static GLuint loadBitmap(char *fname)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID
	// load file - using core SDL library
	SDL_Surface *tmpSurface = SDL_LoadBMP(fname);

	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
	externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}
	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
	externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID; // return value of texture ID
}

Skybox::Skybox()
{


	// front
	faces[0] = new Renderable(vertices,textCoord,indices,indiceCount,vertexCount);
	faces[0]->setTexture( loadBitmap("Front.bmp") );
	//faces[0]->setAngle(180);
	//faces[0]->setRotationalAxis( glm::vec3(0.0f,0.0f,1.0f) );
	

	// right
	faces[1] = new Renderable(vertices,textCoord,indices,indiceCount,vertexCount);
	faces[1]->setTexture( loadBitmap("right.bmp") );

	faces[1]->setAngle(90);
	faces[1]->setRotationalAxis( glm::vec3(0.0f,1.0f,0.0f) );
	

	// back
	faces[2] =new Renderable(vertices,textCoord,indices,indiceCount,vertexCount);
	faces[2]->setTexture( loadBitmap("back.bmp") );

	faces[2]->setAngle(90);
	faces[2]->setRotationalAxis( glm::vec3(0.0f,1.0f,0.0f) );
	

	// left
	faces[3] =new Renderable(vertices,textCoord,indices,indiceCount,vertexCount);
	faces[3]->setTexture( loadBitmap("left.bmp") );

	faces[3]->setAngle(90);
	faces[3]->setRotationalAxis( glm::vec3(0.0f,1.0f,0.0f) );
	

	//down
	faces[4] =new Renderable(vertices,textCoord,indices,indiceCount,vertexCount);
	faces[4]->setTexture( loadBitmap("down.bmp") );
	
	faces[4]->setRotationalAxis(glm::vec3(0.0f,0.0f,1.0f));
	faces[4]->setAngle(90);


	//up
	faces[5] = new Renderable(vertices,textCoord,indices,indiceCount,vertexCount);
	faces[5]->setTexture( loadBitmap("up.bmp") );

	faces[5]->setRotationalAxis(glm::vec3(0.0f,0.0f,1.0f));
	faces[5]->setAngle(90);

	for(int i = 0 ;i < 6; i++)
		faces[i]->getCantMove() = faces[i]->getCantScale() = true;

	
	


}


void Skybox::render(GLuint& skyboxShader,glm::mat4 & matrix,glm::mat4 & projection,GLuint& mvp)
{
	
	//rt3d::setUniformMatrix4fv(mvpShader, "projection", glm::value_ptr(matrix));

	
	

	glUseProgram(skyboxShader);
	glDepthMask(GL_FALSE); // make sure depth test is off
	rt3d::setUniformMatrix4fv(skyboxShader, "projection", glm::value_ptr(projection));

	glm::mat4 rotationOnlyMatrix = glm::mat4(glm::mat3(matrix));
	
	
	rotationOnlyMatrix = glm::mat4(glm::mat3(glm::rotate(rotationOnlyMatrix,180.0f,glm::vec3(0.0f,0.0f,1.0f))));

	
	faces[0]->drawWithRefMat(skyboxShader,rotationOnlyMatrix);


	
	faces[1]->drawWithRefMat(skyboxShader,rotationOnlyMatrix);
	
	
	faces[2]->drawWithRefMat(skyboxShader,rotationOnlyMatrix);
	faces[3]->drawWithRefMat(skyboxShader,rotationOnlyMatrix);
	//faces[4]->drawWithRefMat(skyboxShader,rotationOnlyMatrix);
	//faces[5]->drawWithRefMat(skyboxShader,rotationOnlyMatrix);

	glDepthMask(GL_TRUE); // make sure depth test is on
	glUseProgram(mvp); // reset shader
	
	
}