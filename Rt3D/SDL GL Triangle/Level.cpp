#include "Level.h"

#include "Game.h"
#include <sstream>

int Level::buildingsPlaced = 0;

Level::Level(std::hash_map<std::string,Renderable> &objs)
{
	renderableObjects = objs;
	skybox = new Skybox();
	activeCamera = "Free";
	lightPosition = glm::vec4(1.0f,20.0f,1.0f,1.0f);
	buildingTemplate = nullptr;
	canMove = true;
}

void Level::handleKeyBoardInput(Uint8 * keys)
{
	if( keys[SDL_SCANCODE_P] ) activeCamera = "Follow";
	if( keys[SDL_SCANCODE_F] ) activeCamera = "Free";

	if( keys[SDL_SCANCODE_L] ) 	
		activeCamera = "Fixed";
	
	if( keys[SDL_SCANCODE_B] )  // switch to build mode
	{
		activeCamera = "BuildCamera";
		buildingTemplate =new Renderable(5.0f,4.0f,5.0f,"Cube.obj");
		buildingTemplate->setTexture( Game::getTextures()["BuildingFace"]);
		buildingTemplate->setScale(glm::vec3(2.0f,4.0f,2.0f));
		buildingTemplate->setMaterial(std::string("Transparent"));

		buildingCameraDistance = 10.0f;
		placeable = false;
	}


	if( activeCamera.compare("Free") == 0) // switch camera to free mode
	{
		renderableObjects["Player"].getCurrentAnim() = 10;
		if( keys[SDL_SCANCODE_RIGHT] ) cameras[activeCamera].getAngle() += 3.0f;
                
		if( keys[SDL_SCANCODE_LEFT] ) cameras[activeCamera].getAngle() -= 3.0f;
                
		if( keys[SDL_SCANCODE_Z] )  cameras[activeCamera].getEye().y += 1.0f;     
		if( keys[SDL_SCANCODE_X] ) 	cameras[activeCamera].getEye().y  -= 1.0f;


		if( keys[SDL_SCANCODE_W] ) cameras[activeCamera].moveZPlane(-0.1f);
		if( keys[SDL_SCANCODE_S] ) cameras[activeCamera].moveZPlane(0.1f);
		if( keys[SDL_SCANCODE_D] ) cameras[activeCamera].moveXPlane(-0.1f);
		if( keys[SDL_SCANCODE_A] ) cameras[activeCamera].moveXPlane(0.1f);
	}
	



	else if( activeCamera.compare("Follow") == 0) //camera follows the player
	{
		bool hasMoved = false;
		if( keys[SDL_SCANCODE_RIGHT] ) cameras[activeCamera].getAngle() += 3.0f;

		if( keys[SDL_SCANCODE_LEFT] )  cameras[activeCamera].getAngle() -= 3.0f;
		
		if( keys[SDL_SCANCODE_UP]  && cameras[activeCamera].getEye().y < 40.0f )  cameras[activeCamera].getEye().y += 1.0f;     
		if( keys[SDL_SCANCODE_DOWN] && cameras[activeCamera].getEye().y > 1.0f) cameras[activeCamera].getEye().y  -= 1.0f;

		if( keys[SDL_SCANCODE_W] && canMove )
		{hasMoved = true; renderableObjects["Player"].getAngle() = 90.0f;   renderableObjects["Player"].getPosition() = Camera::moveXPlane(renderableObjects["Player"].getPosition(),renderableObjects["Player"].getAngle(),-1.0f); }
		else if( keys[SDL_SCANCODE_S] && canMove) { hasMoved = true; renderableObjects["Player"].getAngle() = 270;   renderableObjects["Player"].getPosition() = Camera::moveXPlane(renderableObjects["Player"].getPosition(),renderableObjects["Player"].getAngle(),-1.0f);}
		else if( keys[SDL_SCANCODE_D] && canMove) {hasMoved = true; renderableObjects["Player"].getAngle() = 0.0f;   renderableObjects["Player"].getPosition() = Camera::moveXPlane(renderableObjects["Player"].getPosition(),renderableObjects["Player"].getAngle(),1.0f);}
		else if( keys[SDL_SCANCODE_A] && canMove) {hasMoved = true; renderableObjects["Player"].getAngle() = 180.0f;   renderableObjects["Player"].getPosition() = Camera::moveXPlane(renderableObjects["Player"].getPosition(),renderableObjects["Player"].getAngle(),1.0f);}		
		
		
		if( hasMoved) renderableObjects["Player"].getCurrentAnim() = 1;
		else renderableObjects["Player"].getCurrentAnim() = 0;


	} 




	else if(activeCamera.compare("BuildCamera") ==0 ) // building mode
	{
		float moveSpeed = 1.0f;

		if( keys[SDL_SCANCODE_RETURN] && placeable)
		{
			activeCamera ="Free";
			std::ostringstream stream;
			stream << ++buildingsPlaced;
			buildingTemplate->setMaterial(std::string("Default") );
			renderableObjects["Building"+ stream.str()] = *buildingTemplate;
			buildingTemplate = nullptr;
			return;
		}

		if( keys[SDL_SCANCODE_RSHIFT] || keys[SDL_SCANCODE_LSHIFT] ) moveSpeed = 0.1f;
		if( keys[SDL_SCANCODE_RIGHT] ) cameras[activeCamera].getAngle() += 3.0f;
	
		if( keys[SDL_SCANCODE_LEFT] ) cameras[activeCamera].getAngle() -= 3.0f;
	
		

		//scale building overall
		if ( keys[SDL_SCANCODE_COMMA] && buildingCameraDistance < 17.0f ) 
		{buildingTemplate->getScale() += glm::vec3(0.05f,0.05f,0.05f); buildingTemplate->getPosition() +=glm::vec3(0.05f,0.05f,0.05f); buildingCameraDistance += 0.15f; }
		if ( keys[SDL_SCANCODE_PERIOD] && buildingCameraDistance > 7.0f)
		{buildingTemplate->getScale() -= glm::vec3(0.05f,0.05f,0.05f);buildingTemplate->getPosition() -=glm::vec3(0.05f,0.05f,0.05f); buildingCameraDistance -= 0.15f; }
	

		// scale building along x axis
		if ( keys[SDL_SCANCODE_1] && buildingTemplate->getScale().x > 0.3)
		{buildingTemplate->getScale() -= glm::vec3(0.05f,0.0f,0.00f);buildingTemplate->getPosition() -=glm::vec3(0.05f,0.0f,0.0f); }
		if ( keys[SDL_SCANCODE_2] && buildingTemplate->getScale().x < 5)
		{buildingTemplate->getScale() += glm::vec3(0.05f,0.0f,0.00f);buildingTemplate->getPosition() +=glm::vec3(0.05f,0.0f,0.0f); }

		//scale building along z axis
		if ( keys[SDL_SCANCODE_3] && buildingTemplate->getScale().z > 0.3)
		{buildingTemplate->getScale() -= glm::vec3(0.0f,0.0f,0.05f);buildingTemplate->getPosition() -=glm::vec3(0.0f,0.0f,0.05f); }
		if ( keys[SDL_SCANCODE_4] && buildingTemplate->getScale().z < 5)
		{buildingTemplate->getScale() += glm::vec3(0.0f,0.0f,0.05f);buildingTemplate->getPosition() +=glm::vec3(0.0f,0.0f,0.05f); }



		if( keys[SDL_SCANCODE_Z] ) { buildingTextureIndex +1 < Game::getBuildingTextures().size() ? ++buildingTextureIndex : buildingTextureIndex =0; buildingTemplate->setTexture(Game::getBuildingTextures()[buildingTextureIndex]);}
		if( keys[SDL_SCANCODE_X] ) { buildingTextureIndex - 1 >= 0 ? --buildingTextureIndex : buildingTextureIndex = Game::getBuildingTextures().size()-1 ; buildingTemplate->setTexture(Game::getBuildingTextures()[buildingTextureIndex]);}

		if( keys[SDL_SCANCODE_UP] ) cameras[activeCamera].getEye().y += 1.0f;     
		if( keys[SDL_SCANCODE_DOWN] ) cameras[activeCamera].getEye().y  -= 1.0f;


		if( keys[SDL_SCANCODE_W] ) buildingTemplate->getPosition() = Camera::moveZPlane(buildingTemplate->getPosition(),0.0f,moveSpeed);
		if( keys[SDL_SCANCODE_S] ) buildingTemplate->getPosition() = Camera::moveZPlane(buildingTemplate->getPosition(),0.0f,-moveSpeed);
		if( keys[SDL_SCANCODE_D] ) buildingTemplate->getPosition() = Camera::moveXPlane(buildingTemplate->getPosition(),0.0f,moveSpeed);
		if( keys[SDL_SCANCODE_A] ) buildingTemplate->getPosition() = Camera::moveXPlane(buildingTemplate->getPosition(),0.0f,-moveSpeed);		
		if( keys[SDL_SCANCODE_Q] ) renderableObjects["Player"].getAngle()--;
		if( keys[SDL_SCANCODE_E] ) renderableObjects["Player"].getAngle()++;
	}
}


void Level::draw()
{
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,101.0f);

	glm::vec3 focus ;

	if( activeCamera.compare("Free") == 0 ) // decides where the camera should focus   <---- needs refactored
		focus =  Camera::moveZPlane(cameras["Free"].getEye(),cameras["Free"].getAngle(),-1.0f);
	else if(activeCamera.compare("Follow") ==0  )
	{
		focus = renderableObjects["Player"].getPosition();
		float y = cameras[activeCamera].getEye().y;
		cameras[activeCamera].getEye() = Camera::moveZPlane( renderableObjects["Player"].getPosition(),cameras[activeCamera].getAngle(),5.0f) ;
		cameras[activeCamera].getEye().y = y;
	} else if( activeCamera.compare("Fixed") == 0)
	{
		focus = glm::vec3(0.0f,0.0f,0.0000000000000001f);
	} else if( activeCamera.compare("BuildCamera") == 0)
	{
		focus = buildingTemplate->getPosition();
		float y = cameras[activeCamera].getEye().y;
		cameras[activeCamera].getEye() = Camera::moveZPlane( buildingTemplate->getPosition(),cameras[activeCamera].getAngle(),buildingCameraDistance) ;
		cameras[activeCamera].getEye().y = y;
	}



	cameras[activeCamera].focusOn( focus  );
	skybox->render(Game::getShaders()["Skybox"],cameras[activeCamera].look(),projection,Game::getActiveShader());


	rt3d::setUniformMatrix4fv(Game::getActiveShader(), "projection", glm::value_ptr(projection));


	std::hash_map<std::string,Renderable>::iterator it = renderableObjects.begin();

	rt3d::setLightPos(Game::getActiveShader(),glm::value_ptr( (cameras[activeCamera].look()*lightPosition) ));
	if( buildingTemplate != nullptr ) buildingTemplate->draw( Game::getActiveShader(),cameras[activeCamera].look() );
	for(; it != renderableObjects.end(); it++)
	{

		it->second.draw(Game::getActiveShader(),cameras[activeCamera].look());
	}




}


void Level::update()
{
	std::hash_map<std::string,Renderable>::iterator it = renderableObjects.begin();

	bool collide = false,playerCollide = false;; // if it has collided at all 


	

	for(; it != renderableObjects.end(); it++)
	{
		
		it->second.update(cameras[activeCamera]);

		if( buildingTemplate != nullptr && it->first.size() >= 8)
		{

			std::string str(""); // extract building 
			for(int i = 0; i < it->first.size(), i < 8; i++)
			{
				str += it->first[i];
			}
		
			
			if( str.compare("Building") == 0 && buildingTemplate->checkCollision(it->second) )
				{
				
					buildingTemplate->setMaterial(std::string("TransparentRed") );
					collide = true;
				}
		}

	}


	placeable = !collide;
	canMove = !playerCollide;

	if( buildingTemplate != nullptr && !collide) // if nothing has collided
	{
		buildingTemplate->setMaterial(std::string("Transparent"));
	}
}

