#ifndef SKYBOX_H
#define SKYBOX_H
#include "Renderable.h"

class Skybox
{
public:
	Skybox();
	void render(GLuint& skyboxShader,glm::mat4 & matrix,glm::mat4 & projection,GLuint & thisWillAutomaticlySetAfterUseInThisMethodShader); 


private:
	Renderable *faces[6];

	static GLfloat vertices[];
	static GLfloat textCoord[];
	static GLuint indices[];
	static GLuint vertexCount,indiceCount;

};

#endif