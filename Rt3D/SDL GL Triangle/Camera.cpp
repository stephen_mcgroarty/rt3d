#include "Camera.h"

#define DEG_TO_RAD 0.017453293


Camera::Camera()
{
	eye = center = up =glm::vec3(0.0f,1.0f,0.0f);
	angle = 0;
}

Camera::Camera(glm::vec3 nEye,glm::vec3 nCenter,glm::vec3 nUp)
{
	eye = nEye;
	center = nCenter;
	up = nUp;
	angle = 0;
}

void Camera::focusOn(glm::vec3 nCenter)
{
	center = nCenter;
}

glm::mat4 Camera::look()
{
	return glm::lookAt(eye,center,up);
}

void Camera::moveTo(glm::vec3 newPosition)
{
	eye = newPosition;
}



void Camera::moveZPlane( float d)
{

	eye = glm::vec3(eye.x + d*std::sin(angle*DEG_TO_RAD),
		eye.y, eye.z - d*std::cos(angle*DEG_TO_RAD));

}

void Camera::moveXPlane( float d)
{
	eye = glm::vec3(eye.x + d * std::cos(angle* DEG_TO_RAD),eye.y, eye.z + d * std::sin(angle*DEG_TO_RAD) );
}



glm::vec3 Camera::moveZPlane(glm::vec3 cam,float angle,float d)
{
	 return glm::vec3(cam.x + d*std::sin(angle*DEG_TO_RAD),
		cam.y, cam.z - d*std::cos(angle*DEG_TO_RAD));
}

glm::vec3 Camera::moveXPlane(glm::vec3 cam,float angle,float d)
{
	 return  glm::vec3(cam.x + d * std::cos(angle* DEG_TO_RAD),cam.y, cam.z + d * std::sin(angle*DEG_TO_RAD) );
}