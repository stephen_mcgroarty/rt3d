#ifndef RENDERABLE_H
#define RENDERABLE_H
#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <map>


#include "Camera.h"
#include "md2model.h"

class Camera;

class Renderable
{
public:
	Renderable() { }
	Renderable(GLfloat * vertices,GLfloat * texCoords,GLuint * indices,GLuint indiceCount,GLuint vertexCount);
	Renderable(GLfloat x,GLfloat y,GLfloat z,std::string fileName);


	void draw(GLuint& mvpShaderProgram,glm::mat4 );
	void drawWithRefMat(GLuint &mvpShaderProgram,glm::mat4 & matrix); // same as other draw just with matrix passed by refrence

	void setScale(glm::vec3 scale);
	void setTexture(GLuint tex);
	void moveTo(float newX,float newY,float newZ,float newAngle);
	void setAngle(float angle);
	void setRotationalAxis( glm::vec3 axis);
	void update(Camera cam);
	GLfloat &getX() { return position.x; }
	GLfloat &getY() { return position.y; } 
	GLfloat &getZ() { return position.z; }
	glm::vec3 &getPosition() { return position; }
	void handleInput(Uint8 * keys);

	GLfloat & getDx() { return dx; }
	GLfloat & getDy() { return dy; }
	GLfloat & getDz() { return dz; }

	float & getAngle() { return angle; }


	bool checkCollision(Renderable & col);
	bool checkCollisionAnim(Renderable & col);
	static Renderable buildingTemplate(GLfloat x,GLfloat y,GLfloat z);


	//Renderable *& getAttachedObject() { return attachedObject; }
	int & getCurrentAnim() { return currentAnimation; }
	glm::vec3 & getScale() { return scale;}
	void setMaterial(std::string & materialID);
	md2model *& getModel() { return model; }
	bool & getCantScale() {return cantScale; }
	bool & getCantMove() { return cantMove; }
private:
	glm::vec4 & getBox(Renderable & col);
	GLuint texture;
	GLfloat angle;
	glm::vec3 position;
	GLfloat dx,dy,dz;
	glm::vec3 scale;
	GLuint indiceCount,vertexCount;
	GLuint mesh;
	bool cantMove,cantScale;
	static std::map<std::string,std::pair<GLuint,GLuint>> map;
	glm::vec3 rotationalAxis;
	md2model *model;	
	std::string  material; // we can compare strings easier that materialStructs
	
	int currentAnimation;
	void initObj(std::string);
	void initMd2(std::string);
};






#endif